local S = ctf.S
return {
  teams = {
    red     = {colorstr="#FF0000",colornum=0xFF0000,desc = S"Fire"},
    green   = {colorstr="#00FF00",colornum=0x00FF00,desc = S"Earth"},
    blue    = {colorstr="#0000FF",colornum=0x0000FF,desc = S"Water"},
    cyan    = {colorstr="#00FFFF",colornum=0x00FFFF,desc = S"Sky"},
    magenta = {colorstr="#FF00FF",colornum=0xFF00FF,desc = S"Magic"},
    yellow  = {colorstr="#FFFF00",colornum=0xFFFF00,desc = S"Sun"},
  },
  CRYSTALS_PER_TEAM = 4,
  PLAYERS_PER_TEAM = 5,
  SPAWNPOINT = {x=0,y=0,z=0},
  SPAWN_OFFSET = {x=0,y=128,z=0},
  FIELD_RADIUS = 128,
  BORDER_RADIUS = 5,
  SPAWN_DIST = 20000,
  TEAM_DIST = 50,
  TELEPORT_ROOM = {x=7.5,y=20000+7.5,z=7.5},
  TELEPORT_ROOM_RADIUS = 64,
  PLAYER_DIST = 3,
  SPAWNFALL_LIMIT = 128
}