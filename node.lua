-- LUALOCALS < ---------------------------------------------------------
local nodecore, minetest
    = nodecore, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function reg(name,color)
  minetest.register_node(modname..":crystal_"..name,{
    drawtype = "mesh",
    paramtype = "light",
    sunlight_propagates = true,
    light_source = 14,
    mesh = "nc_ctf_crystal.obj",
    groups = {crystal = 1,cracky=4,falling_node = 1},
		selection_box = {
      type = "fixed",
      fixed = {
        {-0.3,-0.5,-0.3,  0.3,0.5,0.3},
      }
    },
    tiles = {
      {name = "(nc_ctf_crystal_base.png^[multiply:"..color..")^nc_ctf_crystal_shine.png",
        animation={
          type = "vertical_frames",

          aspect_w = 64,
          aspect_h = 64,

          length = 1,
        }
      }
    },
    sounds = nodecore.sounds("nc_optics_glassy")
  })

  nodecore.register_ambiance({
      label = "Crystal Ambiance",
      nodenames = {modname..":crystal_"..name},
      interval = 4,
      chance = 4,
      sound_name = "nc_ctf_weird",
      check = function(pos)
        return {gain = 1,pos=pos}
      end
    })
end

function ctf.crystals(teams)
  for name, team in pairs(teams) do
    reg(name,team.colorstr)
    for name2, team2 in pairs(teams) do
      if name2 ~= name then
        local function after()
          local team1,team2 = ctf.teams[name],ctf.teams[name2]
          team1.score = team1.score-1
          team2.score = team2.score+1
          ctf.check_win()
        end
        nodecore.register_craft({
            label = "claim crystal",
            nodes = {
              {match = modname .. ":crystal_"..name, replace = modname..":crystal_"..name2},
              {x = 1, match = modname .. ":crystal_"..name2}
            },
            after = after
          })
        nodecore.register_craft({
            label = "claim crystal",
            nodes = {
              {match = modname .. ":crystal_"..name, replace = modname..":crystal_"..name2},
              {y = 1, match = modname .. ":crystal_"..name2}
            },
            after = after
          })
        nodecore.register_craft({
            label = "claim crystal",
            nodes = {
              {match = modname .. ":crystal_"..name, replace = modname..":crystal_"..name2},
              {y = -1, match = modname .. ":crystal_"..name2}
            },
            after = after
          })
      end
    end
  end
end

minetest.register_node(modname..":telebarrier",{
    light_source = 14,
    paramtype = "light",
    drawtype="normal",
    pointable = false,
    tiles = {
      {name = "nc_ctf_telebarrier.png",
        animation={
          type = "vertical_frames",

          aspect_w = 16,
          aspect_h = 16,

          length = 3,
        },
        align_style = "world",
        scale=16
      }
    },
  })

minetest.register_node(modname..":barrier",{
    light_source = 14,
    paramtype = "light",
    drawtype="normal",
    pointable = false,
    use_texture_alpha=true,
    tiles = {
      {name = "nc_ctf_barrier.png",
        animation={
          type = "vertical_frames",

          aspect_w = 16,
          aspect_h = 16,

          length = 3,
        },
        align_style = "world",
        scale=16
      }
    },
  })

local bar, tbar = modname..":barrier",modname..":telebarrier"